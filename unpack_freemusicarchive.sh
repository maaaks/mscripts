#!/bin/sh

# Unpacks downloaded archives from FreeMusicArchive.org
# into /Artist/Album/Track.mp3 structure.

# Version 1.0
# 15.01.2013

# Constants
MY_LIBRARY=/media/data/Music/MyLibrary

# Start iterating
for argument in "$@"
do
	# If a file given, use it directly
	# If a directory given, get all files in it
	# In other cases, don't use the path
	if   [ -f $path ]; then
		paths=$argument
	elif [ -d $path ]; then
		paths=`find $argument -maxdepth 1 -type f`
	else
		echo "Incorrect input path: $argument"
		continue
	fi
	
	# Currently, $paths contains one or more _files_ to be unpacked
	for path in $paths
	do
		# Create a temporary directory
		tmpdir=`mktemp -d`
		
		# Create a subfolder named with artist name,
		# containing a subfolder named with album title
		artist=`echo $path | sed -e "s/_-_.*//g" -e "s/_/ /g"`
		album=`echo $path | sed -e "s/.*_-_//g" -e "s/_/ /g" -e "s/.zip//g"`
		mkdir --parents "$MY_LIBRARY/$artist/$album"
		
		# Unpack ZIP there
		unzip $path -d "$MY_LIBRARY/$artist/$album"
		
		# Rename unpacked tracks (remove artist name and track index)
		echo "Renaming unpacked files..."
		for track in `ls "$MY_LIBRARY/$artist/$album"`
		do
			newname=`echo $track | sed -e "s/.*_-_//g" -e "s/_/ /g" -e "s/.mp3//g"`
			mv "$MY_LIBRARY/$artist/$album/$track" "$MY_LIBRARY/$artist/$album/$newname.mp3"
		done
	done
done