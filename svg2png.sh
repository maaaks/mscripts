#!/bin/bash


# Функция для отображения вопроса "Да/Нет"
yesno ()
{
	if [ -x /usr/bin/kdialog ] ; then
		kdialog --title "$2" --yesno "$1"
	elif [ -x /usr/bin/zenity ] ; then
		zenity --question --title="$2" --text="$1"
	else
		xmessage "Please install \"kdialog\" (for KDE) or \"zenity\" (for GNOME) and restart this script."
		exit
	fi
	return $?
}

# Функция, возвращающая список (через пробел) нескольких файлов по выбору пользователя
selectmultiplefiles ()
{
	if [ -x /usr/bin/kdialog ] ; then
		return=`kdialog --title "$1" --multiple --getopenfilename "./" "*"`
	elif [ -x /usr/bin/zenity ] ; then
		return=`zenity --title="$1" --file-selection --multiple --separator=" "`
	else
		xmessage "Please install \"kdialog\" (for KDE) or \"zenity\" (for GNOME) and restart this script."
		exit
	fi
	echo $return
}

# Функция для выбора папки
selectfolder ()
{
	if [ -x /usr/bin/kdialog ] ; then
		return=`kdialog --title "$1" --getexistingdirectory ./`
	elif [ -x /usr/bin/zenity ] ; then
		return=`zenity --title="$1" --file-selection --directory`
	else
		xmessage "Please install \"kdialog\" (for KDE) or \"zenity\" (for GNOME) and restart this script."
		exit
	fi
	echo $return
}

# Функция для отображения окна с прогрессбаром
showprogress ()
{
	#if [ -x /usr/bin/kdialog ] ; then
	#	kdialog --title "$1" --progressbar "$1" "100"
	if [ -x /usr/bin/zenity ] ; then
		zenity --title="$1" --progress --text="$1" --percentage=0
	else
		xmessage "Please install \"kdialog\" (for KDE) or \"zenity\" (for GNOME) and restart this script."
		exit
	fi
}






# Спрашиваем, туда ли попал пользователь
yesno $'Вас приветствует мастер конвертирования файлов SVG в картинки PNG.\n\nВам будет предложено выбрать SVG-файлы, которые вы хотите преобразовать, и папку, в которую вы хотите поместить готовые файлы PNG.\n\nНажмите "Да", чтобы начать работу, или "Нет", чтобы закрыть этот мастер.' "Конвертер SVG в PNG"
if [ $? != "0" ] ; then
	exit
fi

# Просим его выбрать файлики
filenames=`selectmultiplefiles "Выберите файлы SVG для преобразования"`

# Просим его выбрать папочку
outputfolder=`selectfolder "Выберите папку для сохранения файлов PNG"`

# Вы будете смеяться - я не знаю, как ещё можно сосчитать количество выбранных файлов...
for i in $filenames
do
	count=$(( $count+1 ))
done




( for filenamesvg in $filenames
do
	# Попробуем убрать у файла расширение SVG и приписать PNG
	filename_no_svg=`basename $filenamesvg .svg`
	filename_no_SVG=`basename $filenamesvg .SVG`

	if [ "$filename_no_svg" != "$filename" ] ; then
		# Расширение было набрано маленькими буквами
		filename_png="${filename_no_svg}.png"
	elif [ "$filename_no_SVG != $filename" ] ; then
		# Расширение было набрано большими буквами
		filename_png="${filename_no_SVG}.png"
	else
		# Хрен его поймёт что там с расширением, поэтому тупо припишем ".png"
		filename_png="${filename}.png"
	fi
	
	# Двигаем прогрессбар
	currentprogress=$(( $currentprogress + 1 ))
	echo $(( $currentprogress * 50 / $count ))

	# Теперь вызываем Inkscape и просим его сохранить SVG в PNG
	inkscape "$filenamesvg" --export-png="$outputfolder/$filename_png"
	
	# Двигаем прогрессбар
	currentprogress=$(( $currentprogress + 1 ))
	echo $(( $currentprogress * 50 / $count ))
done
) | showprogress "Преобразование SVG в PNG..."